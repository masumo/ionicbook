/**
 * このファイルはライセンスフリーです。ご自身の責任の下、ご自由にお使いください。
 */

angular.module('starter.controllers', [])

.controller('DashCtrl', function($scope) {})

.controller('ChatsCtrl', function($scope, Chats) {
  // With the new view caching in Ionic, Controllers are only called
  // when they are recreated or on app start, instead of every page change.
  // To listen for when this page is active (for example, to refresh data),
  // listen for the $ionicView.enter event:
  //
  //$scope.$on('$ionicView.enter', function(e) {
  //});

  $scope.chats = Chats.all();
  $scope.remove = function(chat) {
    Chats.remove(chat);
  };
})

.controller('ChatDetailCtrl', function($scope, $stateParams, Chats) {
  $scope.chat = Chats.get($stateParams.chatId);
})

.controller('AccountCtrl', function($scope, $ionicScrollDelegate) {
  $scope.settings = {
    enableFriends: true
  };

  $scope.scrollToTop = function() {
    console.log('描画開始位置へスクロールします。');
    $ionicScrollDelegate.$getByHandle('scrollView').scrollTop();
  };

  $scope.scroolTo = function(x, y) {
    console.log('(' + x + ',' + y +')に向けてスクロールします。');
    $ionicScrollDelegate.$getByHandle('scrollView').scrollTo(x, y, true);
  };

  $scope.scroolBy = function(x, y) {
    console.log('現在の場所から(' + x + ',' + y +')だけスクロールします。');
    $ionicScrollDelegate.$getByHandle('scrollView').scrollBy(x, y, true);
  };

  $scope.zoomTo = function(zoomLevel) {
    if(isNaN(zoomLevel)) { // 数字ではなかった場合、デフォルトの数値を使う
      zoomLevel = 1;
    }
    console.log('(0, 0)の方向に' + zoomLevel + '倍のズーム状態にします。');
    $ionicScrollDelegate.$getByHandle('scrollView').zoomTo(zoomLevel, true, 0, 0);
  };

  $scope.zoomBy = function(zoomLevel) {
    if(isNaN(zoomLevel)) { // 数字ではなかった場合、デフォルトの数値を使う
      zoomLevel = 1;
    }
    console.log('(0, 0)の方向に今よりも' + zoomLevel + '倍のズームをします。');
    $ionicScrollDelegate.$getByHandle('scrollView').zoomBy(zoomLevel, true, 0, 0);
  };
});
